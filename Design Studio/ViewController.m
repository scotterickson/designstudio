//
//  ViewController.m
//  Design Studio
//
//  Created by Erik Stadler on 1/15/13.
//  Copyright (c) 2013 Erik Stadler. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	[_webView setScalesPageToFit:YES];
    
    self.view.backgroundColor = [UIColor blackColor];
	[self setNeedsStatusBarAppearanceUpdate];
    
	//[_webView loadRequest:[NSURLRequest requestWithURL:
	//	[NSURL URLWithString:@"http://www.touchpros.com/dev/bathplanet/www/index_ipad.html"]]];
	
	//NSString *core = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"ipad_bathplanet/index.html"];
	
	NSString *core = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"ipad_bathplanet/index.html"];
	
	[_webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:core]]];
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)webViewDidStartLoad:(UIWebView *)webView {
	//[webView stringByEvaluatingJavaScriptFromString:@"window.location.href"];
	[_activityIndicatorView startAnimating];
	[_activityIndicatorView setHidden:NO];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	[_activityIndicatorView stopAnimating];
	[_activityIndicatorView setHidden:YES];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    // Open Specific Pages, and External Links (not beginning with http://www.example.com) in Safari.app
    if (
        (navigationType == UIWebViewNavigationTypeLinkClicked) &&
        ([[[request URL] absoluteString] hasPrefix:@"http://www.bathplanet.com/quote/"])) {
        
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	return (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
			toInterfaceOrientation == UIInterfaceOrientationLandscapeRight );
}


@end
