//
//  AppDelegate.h
//  Design Studio
//
//  Created by Erik Stadler on 1/15/13.
//  Copyright (c) 2013 Erik Stadler. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
