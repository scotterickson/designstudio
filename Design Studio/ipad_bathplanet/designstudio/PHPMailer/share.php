<?php

require_once('class.phpmailer.php');
//include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch

$mail->IsSMTP(); // telling the class to use SMTP

$to = $_POST['email_to'];
$from = $_POST['email_from'];
$fromName = $_POST['email_from_name'];
$message = $_POST['message'];
$imagePath = $_POST['imgdesign'];
try {
    $mail->Host       = "mail2.vcllc.net"; // SMTP server
    $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
    $mail->SMTPAuth   = true;                  // enable SMTP authentication
    $mail->Host       = "mail2.vcllc.net"; // sets the SMTP server
    $mail->Port       = 25;                    // set the SMTP port for the GMAIL server
    $mail->Username   = "serickson@vcllc.net"; // SMTP account username
    $mail->Password   = "ontario14ONT";        // SMTP account password
    $mail->AddAddress($to, '');
    $mail->AddReplyTo($from, $fromName);
    $mail->SetFrom('info@touchpros.com', $fromName);

    $mail->Subject = 'My new bath design';
    //$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
    $mail->MsgHTML($message.'<br><br><br>'.'<a href="http://dev.touchpros.com/bci/designstudio/"><img src="'.$imagePath.'"></a>');
    //$mail->AddAttachment('images/phpmailer.gif');      // attachment
    $mail->Send();
    echo true;
} catch (phpmailerException $e) {
    echo $e->errorMessage(); //Pretty error messages from PHPMailer
} catch (Exception $e) {
    echo $e->getMessage(); //Boring error messages from anything else!
}