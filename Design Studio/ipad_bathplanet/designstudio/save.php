<?php
	// requires php5
	define('UPLOAD_DIR', 'uploads/');
	$img = $_POST['data'];
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
        $fileName = uniqid() . '.png';
	$file = UPLOAD_DIR . $fileName;
	$success = file_put_contents($file, $data);
	print $success ? $fileName : 'Unable to save the file.';