//
//  ViewController.h
//  Design Studio
//
//  Created by Erik Stadler on 1/15/13.
//  Copyright (c) 2013 Erik Stadler. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end
